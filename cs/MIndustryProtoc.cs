// Generated by ProtoGen, Version=0.9.0.0, Culture=neutral, PublicKeyToken=8fd7408b07f8d2cd.  DO NOT EDIT!

using pb = global::Google.ProtocolBuffers;
using pbc = global::Google.ProtocolBuffers.Collections;
using pbd = global::Google.ProtocolBuffers.Descriptors;
using scg = global::System.Collections.Generic;
namespace ChuMeng {
  
  public static partial class MIndustryProtoc {
  
    #region Extension registration
    public static void RegisterAllExtensions(pb::ExtensionRegistry registry) {
    }
    #endregion
    #region Static variables
    #endregion
    #region Extensions
    internal static readonly object Descriptor;
    static MIndustryProtoc() {
      Descriptor = null;
    }
    #endregion
    
  }
  #region Messages
  public sealed partial class GCPushPlayerEnegry : pb::GeneratedMessageLite<GCPushPlayerEnegry, GCPushPlayerEnegry.Builder> {
    private static readonly GCPushPlayerEnegry defaultInstance = new Builder().BuildPartial();
    public static GCPushPlayerEnegry DefaultInstance {
      get { return defaultInstance; }
    }
    
    public override GCPushPlayerEnegry DefaultInstanceForType {
      get { return defaultInstance; }
    }
    
    protected override GCPushPlayerEnegry ThisMessage {
      get { return this; }
    }
    
    public const int PlayerIdFieldNumber = 1;
    private bool hasPlayerId;
    private long playerId_ = 0L;
    public bool HasPlayerId {
      get { return hasPlayerId; }
    }
    public long PlayerId {
      get { return playerId_; }
      set { SetplayerId(value); }
    }
    private void SetplayerId(long value) {
      hasPlayerId = true;
      playerId_ = value;
    }
    
    public const int EnegryFieldNumber = 2;
    private bool hasEnegry;
    private int enegry_ = 0;
    public bool HasEnegry {
      get { return hasEnegry; }
    }
    public int Enegry {
      get { return enegry_; }
      set { Setenegry(value); }
    }
    private void Setenegry(int value) {
      hasEnegry = true;
      enegry_ = value;
    }
    
    public override bool IsInitialized {
      get {
        return true;
      }
    }
    
    public override void WriteTo(pb::CodedOutputStream output) {
      int size = SerializedSize;
      if (HasPlayerId) {
        output.WriteInt64(1, PlayerId);
      }
      if (HasEnegry) {
        output.WriteInt32(2, Enegry);
      }
    }
    
    private int memoizedSerializedSize = -1;
    public override int SerializedSize {
      get {
        int size = memoizedSerializedSize;
        if (size != -1) return size;
        
        size = 0;
        if (HasPlayerId) {
          size += pb::CodedOutputStream.ComputeInt64Size(1, PlayerId);
        }
        if (HasEnegry) {
          size += pb::CodedOutputStream.ComputeInt32Size(2, Enegry);
        }
        return size;
      }
    }
    
    #region Lite runtime methods
    public override int GetHashCode() {
      int hash = GetType().GetHashCode();
      if (hasPlayerId) hash ^= playerId_.GetHashCode();
      if (hasEnegry) hash ^= enegry_.GetHashCode();
      return hash;
    }
    
    public override bool Equals(object obj) {
      GCPushPlayerEnegry other = obj as GCPushPlayerEnegry;
      if (other == null) return false;
      if (hasPlayerId != other.hasPlayerId || (hasPlayerId && !playerId_.Equals(other.playerId_))) return false;
      if (hasEnegry != other.hasEnegry || (hasEnegry && !enegry_.Equals(other.enegry_))) return false;
      return true;
    }
    
    public override void PrintTo(global::System.IO.TextWriter writer) {
      PrintField("playerId", hasPlayerId, playerId_, writer);
      PrintField("enegry", hasEnegry, enegry_, writer);
    }
    #endregion
    
    public static GCPushPlayerEnegry ParseFrom(pb::ByteString data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static GCPushPlayerEnegry ParseFrom(pb::ByteString data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static GCPushPlayerEnegry ParseFrom(byte[] data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static GCPushPlayerEnegry ParseFrom(byte[] data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static GCPushPlayerEnegry ParseFrom(global::System.IO.Stream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static GCPushPlayerEnegry ParseFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static GCPushPlayerEnegry ParseDelimitedFrom(global::System.IO.Stream input) {
      return CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }
    public static GCPushPlayerEnegry ParseDelimitedFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }
    public static GCPushPlayerEnegry ParseFrom(pb::CodedInputStream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static GCPushPlayerEnegry ParseFrom(pb::CodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static Builder CreateBuilder() { return new Builder(); }
    public override Builder ToBuilder() { return CreateBuilder(this); }
    public override Builder CreateBuilderForType() { return new Builder(); }
    public static Builder CreateBuilder(GCPushPlayerEnegry prototype) {
      return (Builder) new Builder().MergeFrom(prototype);
    }
    
    public sealed partial class Builder : pb::GeneratedBuilderLite<GCPushPlayerEnegry, Builder> {
      protected override Builder ThisBuilder {
        get { return this; }
      }
      public Builder() {}
      
      GCPushPlayerEnegry result = new GCPushPlayerEnegry();
      
      protected override GCPushPlayerEnegry MessageBeingBuilt {
        get { return result; }
      }
      
      public override Builder Clear() {
        result = new GCPushPlayerEnegry();
        return this;
      }
      
      public override Builder Clone() {
        return new Builder().MergeFrom(result);
      }
      
      public override GCPushPlayerEnegry DefaultInstanceForType {
        get { return global::ChuMeng.GCPushPlayerEnegry.DefaultInstance; }
      }
      
      public override GCPushPlayerEnegry BuildPartial() {
        if (result == null) {
          throw new global::System.InvalidOperationException("build() has already been called on this Builder");
        }
        GCPushPlayerEnegry returnMe = result;
        return returnMe;
      }
      
      public override Builder MergeFrom(pb::IMessageLite other) {
        if (other is GCPushPlayerEnegry) {
          return MergeFrom((GCPushPlayerEnegry) other);
        } else {
          base.MergeFrom(other);
          return this;
        }
      }
      
      public override Builder MergeFrom(GCPushPlayerEnegry other) {
        if (other == global::ChuMeng.GCPushPlayerEnegry.DefaultInstance) return this;
        if (other.HasPlayerId) {
          PlayerId = other.PlayerId;
        }
        if (other.HasEnegry) {
          Enegry = other.Enegry;
        }
        return this;
      }
      
      public override Builder MergeFrom(pb::CodedInputStream input) {
        return MergeFrom(input, pb::ExtensionRegistry.Empty);
      }
      
      public override Builder MergeFrom(pb::CodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
        while (true) {
          uint tag = input.ReadTag();
          switch (tag) {
            case 0: {
              return this;
            }
            default: {
              if (pb::WireFormat.IsEndGroupTag(tag)) {
                return this;
              }
              ParseUnknownField(input, extensionRegistry, tag);
              break;
            }
            case 8: {
              PlayerId = input.ReadInt64();
              break;
            }
            case 16: {
              Enegry = input.ReadInt32();
              break;
            }
          }
        }
      }
      
      
      public bool HasPlayerId {
        get { return result.HasPlayerId; }
      }
      public long PlayerId {
        get { return result.PlayerId; }
        set { SetPlayerId(value); }
      }
      public Builder SetPlayerId(long value) {
        result.hasPlayerId = true;
        result.playerId_ = value;
        return this;
      }
      public Builder ClearPlayerId() {
        result.hasPlayerId = false;
        result.playerId_ = 0L;
        return this;
      }
      
      public bool HasEnegry {
        get { return result.HasEnegry; }
      }
      public int Enegry {
        get { return result.Enegry; }
        set { SetEnegry(value); }
      }
      public Builder SetEnegry(int value) {
        result.hasEnegry = true;
        result.enegry_ = value;
        return this;
      }
      public Builder ClearEnegry() {
        result.hasEnegry = false;
        result.enegry_ = 0;
        return this;
      }
    }
    static GCPushPlayerEnegry() {
      object.ReferenceEquals(global::ChuMeng.MIndustryProtoc.Descriptor, null);
    }
  }
  
  public sealed partial class CGEquipIntensify : pb::GeneratedMessageLite<CGEquipIntensify, CGEquipIntensify.Builder> {
    private static readonly CGEquipIntensify defaultInstance = new Builder().BuildPartial();
    public static CGEquipIntensify DefaultInstance {
      get { return defaultInstance; }
    }
    
    public override CGEquipIntensify DefaultInstanceForType {
      get { return defaultInstance; }
    }
    
    protected override CGEquipIntensify ThisMessage {
      get { return this; }
    }
    
    public const int IdFieldNumber = 1;
    private bool hasId;
    private int id_ = 0;
    public bool HasId {
      get { return hasId; }
    }
    public int Id {
      get { return id_; }
      set { Setid(value); }
    }
    private void Setid(int value) {
      hasId = true;
      id_ = value;
    }
    
    public override bool IsInitialized {
      get {
        return true;
      }
    }
    
    public override void WriteTo(pb::CodedOutputStream output) {
      int size = SerializedSize;
      if (HasId) {
        output.WriteInt32(1, Id);
      }
    }
    
    private int memoizedSerializedSize = -1;
    public override int SerializedSize {
      get {
        int size = memoizedSerializedSize;
        if (size != -1) return size;
        
        size = 0;
        if (HasId) {
          size += pb::CodedOutputStream.ComputeInt32Size(1, Id);
        }
        return size;
      }
    }
    
    #region Lite runtime methods
    public override int GetHashCode() {
      int hash = GetType().GetHashCode();
      if (hasId) hash ^= id_.GetHashCode();
      return hash;
    }
    
    public override bool Equals(object obj) {
      CGEquipIntensify other = obj as CGEquipIntensify;
      if (other == null) return false;
      if (hasId != other.hasId || (hasId && !id_.Equals(other.id_))) return false;
      return true;
    }
    
    public override void PrintTo(global::System.IO.TextWriter writer) {
      PrintField("id", hasId, id_, writer);
    }
    #endregion
    
    public static CGEquipIntensify ParseFrom(pb::ByteString data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static CGEquipIntensify ParseFrom(pb::ByteString data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static CGEquipIntensify ParseFrom(byte[] data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static CGEquipIntensify ParseFrom(byte[] data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static CGEquipIntensify ParseFrom(global::System.IO.Stream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static CGEquipIntensify ParseFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static CGEquipIntensify ParseDelimitedFrom(global::System.IO.Stream input) {
      return CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }
    public static CGEquipIntensify ParseDelimitedFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }
    public static CGEquipIntensify ParseFrom(pb::CodedInputStream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static CGEquipIntensify ParseFrom(pb::CodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static Builder CreateBuilder() { return new Builder(); }
    public override Builder ToBuilder() { return CreateBuilder(this); }
    public override Builder CreateBuilderForType() { return new Builder(); }
    public static Builder CreateBuilder(CGEquipIntensify prototype) {
      return (Builder) new Builder().MergeFrom(prototype);
    }
    
    public sealed partial class Builder : pb::GeneratedBuilderLite<CGEquipIntensify, Builder> {
      protected override Builder ThisBuilder {
        get { return this; }
      }
      public Builder() {}
      
      CGEquipIntensify result = new CGEquipIntensify();
      
      protected override CGEquipIntensify MessageBeingBuilt {
        get { return result; }
      }
      
      public override Builder Clear() {
        result = new CGEquipIntensify();
        return this;
      }
      
      public override Builder Clone() {
        return new Builder().MergeFrom(result);
      }
      
      public override CGEquipIntensify DefaultInstanceForType {
        get { return global::ChuMeng.CGEquipIntensify.DefaultInstance; }
      }
      
      public override CGEquipIntensify BuildPartial() {
        if (result == null) {
          throw new global::System.InvalidOperationException("build() has already been called on this Builder");
        }
        CGEquipIntensify returnMe = result;
        return returnMe;
      }
      
      public override Builder MergeFrom(pb::IMessageLite other) {
        if (other is CGEquipIntensify) {
          return MergeFrom((CGEquipIntensify) other);
        } else {
          base.MergeFrom(other);
          return this;
        }
      }
      
      public override Builder MergeFrom(CGEquipIntensify other) {
        if (other == global::ChuMeng.CGEquipIntensify.DefaultInstance) return this;
        if (other.HasId) {
          Id = other.Id;
        }
        return this;
      }
      
      public override Builder MergeFrom(pb::CodedInputStream input) {
        return MergeFrom(input, pb::ExtensionRegistry.Empty);
      }
      
      public override Builder MergeFrom(pb::CodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
        while (true) {
          uint tag = input.ReadTag();
          switch (tag) {
            case 0: {
              return this;
            }
            default: {
              if (pb::WireFormat.IsEndGroupTag(tag)) {
                return this;
              }
              ParseUnknownField(input, extensionRegistry, tag);
              break;
            }
            case 8: {
              Id = input.ReadInt32();
              break;
            }
          }
        }
      }
      
      
      public bool HasId {
        get { return result.HasId; }
      }
      public int Id {
        get { return result.Id; }
        set { SetId(value); }
      }
      public Builder SetId(int value) {
        result.hasId = true;
        result.id_ = value;
        return this;
      }
      public Builder ClearId() {
        result.hasId = false;
        result.id_ = 0;
        return this;
      }
    }
    static CGEquipIntensify() {
      object.ReferenceEquals(global::ChuMeng.MIndustryProtoc.Descriptor, null);
    }
  }
  
  public sealed partial class GCEquipIntensify : pb::GeneratedMessageLite<GCEquipIntensify, GCEquipIntensify.Builder> {
    private static readonly GCEquipIntensify defaultInstance = new Builder().BuildPartial();
    public static GCEquipIntensify DefaultInstance {
      get { return defaultInstance; }
    }
    
    public override GCEquipIntensify DefaultInstanceForType {
      get { return defaultInstance; }
    }
    
    protected override GCEquipIntensify ThisMessage {
      get { return this; }
    }
    
    public override bool IsInitialized {
      get {
        return true;
      }
    }
    
    public override void WriteTo(pb::CodedOutputStream output) {
      int size = SerializedSize;
    }
    
    private int memoizedSerializedSize = -1;
    public override int SerializedSize {
      get {
        int size = memoizedSerializedSize;
        if (size != -1) return size;
        
        size = 0;
        return size;
      }
    }
    
    #region Lite runtime methods
    public override int GetHashCode() {
      int hash = GetType().GetHashCode();
      return hash;
    }
    
    public override bool Equals(object obj) {
      GCEquipIntensify other = obj as GCEquipIntensify;
      if (other == null) return false;
      return true;
    }
    
    public override void PrintTo(global::System.IO.TextWriter writer) {
    }
    #endregion
    
    public static GCEquipIntensify ParseFrom(pb::ByteString data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static GCEquipIntensify ParseFrom(pb::ByteString data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static GCEquipIntensify ParseFrom(byte[] data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static GCEquipIntensify ParseFrom(byte[] data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static GCEquipIntensify ParseFrom(global::System.IO.Stream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static GCEquipIntensify ParseFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static GCEquipIntensify ParseDelimitedFrom(global::System.IO.Stream input) {
      return CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }
    public static GCEquipIntensify ParseDelimitedFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }
    public static GCEquipIntensify ParseFrom(pb::CodedInputStream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static GCEquipIntensify ParseFrom(pb::CodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static Builder CreateBuilder() { return new Builder(); }
    public override Builder ToBuilder() { return CreateBuilder(this); }
    public override Builder CreateBuilderForType() { return new Builder(); }
    public static Builder CreateBuilder(GCEquipIntensify prototype) {
      return (Builder) new Builder().MergeFrom(prototype);
    }
    
    public sealed partial class Builder : pb::GeneratedBuilderLite<GCEquipIntensify, Builder> {
      protected override Builder ThisBuilder {
        get { return this; }
      }
      public Builder() {}
      
      GCEquipIntensify result = new GCEquipIntensify();
      
      protected override GCEquipIntensify MessageBeingBuilt {
        get { return result; }
      }
      
      public override Builder Clear() {
        result = new GCEquipIntensify();
        return this;
      }
      
      public override Builder Clone() {
        return new Builder().MergeFrom(result);
      }
      
      public override GCEquipIntensify DefaultInstanceForType {
        get { return global::ChuMeng.GCEquipIntensify.DefaultInstance; }
      }
      
      public override GCEquipIntensify BuildPartial() {
        if (result == null) {
          throw new global::System.InvalidOperationException("build() has already been called on this Builder");
        }
        GCEquipIntensify returnMe = result;
        return returnMe;
      }
      
      public override Builder MergeFrom(pb::IMessageLite other) {
        if (other is GCEquipIntensify) {
          return MergeFrom((GCEquipIntensify) other);
        } else {
          base.MergeFrom(other);
          return this;
        }
      }
      
      public override Builder MergeFrom(GCEquipIntensify other) {
        if (other == global::ChuMeng.GCEquipIntensify.DefaultInstance) return this;
        return this;
      }
      
      public override Builder MergeFrom(pb::CodedInputStream input) {
        return MergeFrom(input, pb::ExtensionRegistry.Empty);
      }
      
      public override Builder MergeFrom(pb::CodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
        while (true) {
          uint tag = input.ReadTag();
          switch (tag) {
            case 0: {
              return this;
            }
            default: {
              if (pb::WireFormat.IsEndGroupTag(tag)) {
                return this;
              }
              ParseUnknownField(input, extensionRegistry, tag);
              break;
            }
          }
        }
      }
      
    }
    static GCEquipIntensify() {
      object.ReferenceEquals(global::ChuMeng.MIndustryProtoc.Descriptor, null);
    }
  }
  
  #endregion
  
}
