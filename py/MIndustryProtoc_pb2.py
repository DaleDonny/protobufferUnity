# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: MIndustryProtoc.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)


import google.protobuf.csharp_options_pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='MIndustryProtoc.proto',
  package='protocmsg',
  serialized_pb='\n\x15MIndustryProtoc.proto\x12\tprotocmsg\x1a$google/protobuf/csharp_options.proto\"6\n\x12GCPushPlayerEnegry\x12\x10\n\x08playerId\x18\x01 \x01(\x03\x12\x0e\n\x06\x65negry\x18\x02 \x01(\x05\"\x1e\n\x10\x43GEquipIntensify\x12\n\n\x02id\x18\x01 \x01(\x05\"\x12\n\x10GCEquipIntensifyBQ\n\x1c\x63om.td.starfantasy.protocmsgB\x0fMIndustryProtocH\x03\xc2>\t\n\x07\x43huMeng\xc2>\x11\x12\x0fMIndustryProtoc')




_GCPUSHPLAYERENEGRY = _descriptor.Descriptor(
  name='GCPushPlayerEnegry',
  full_name='protocmsg.GCPushPlayerEnegry',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='playerId', full_name='protocmsg.GCPushPlayerEnegry.playerId', index=0,
      number=1, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='enegry', full_name='protocmsg.GCPushPlayerEnegry.enegry', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=74,
  serialized_end=128,
)


_CGEQUIPINTENSIFY = _descriptor.Descriptor(
  name='CGEquipIntensify',
  full_name='protocmsg.CGEquipIntensify',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='protocmsg.CGEquipIntensify.id', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=130,
  serialized_end=160,
)


_GCEQUIPINTENSIFY = _descriptor.Descriptor(
  name='GCEquipIntensify',
  full_name='protocmsg.GCEquipIntensify',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=162,
  serialized_end=180,
)

DESCRIPTOR.message_types_by_name['GCPushPlayerEnegry'] = _GCPUSHPLAYERENEGRY
DESCRIPTOR.message_types_by_name['CGEquipIntensify'] = _CGEQUIPINTENSIFY
DESCRIPTOR.message_types_by_name['GCEquipIntensify'] = _GCEQUIPINTENSIFY

class GCPushPlayerEnegry(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _GCPUSHPLAYERENEGRY

  # @@protoc_insertion_point(class_scope:protocmsg.GCPushPlayerEnegry)

class CGEquipIntensify(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CGEQUIPINTENSIFY

  # @@protoc_insertion_point(class_scope:protocmsg.CGEquipIntensify)

class GCEquipIntensify(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _GCEQUIPINTENSIFY

  # @@protoc_insertion_point(class_scope:protocmsg.GCEquipIntensify)


DESCRIPTOR.has_options = True
DESCRIPTOR._options = _descriptor._ParseOptions(descriptor_pb2.FileOptions(), '\n\034com.td.starfantasy.protocmsgB\017MIndustryProtocH\003\302>\t\n\007ChuMeng\302>\021\022\017MIndustryProtoc')
# @@protoc_insertion_point(module_scope)
